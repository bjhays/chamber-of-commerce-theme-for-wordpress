<?php
  /* 
 Template Name: Home Page
 */
?>
<?php get_header(); ?>

		<div id="container">
			<div id="homepage" role="main">
			<?php $page_id=get_ID_by_slug('home');
				if($page_id!=null){ 
				?>
			<div id="pageOptions">
            <?php $tmp_page_id=get_ID_by_slug('home/about-us');
				if($tmp_page_id!=null){ 
				$tmp_data = get_page( $tmp_page_id ); 
					$tmp_title = $tmp_data->post_title; // Get title
				?> 
                <div id="aboutUs" class="left">
                <h2 style="background: url(<?php echo get_bloginfo('template_url'); ?>/img/aboutUsPuzzle.png) no-repeat; padding-left: 35px; padding-bottom: 16px; padding-top: 6px; margin: 5px;"><?php echo $tmp_title; ?> </h2>
                <p><?php echo apply_filters('the_content', $tmp_data->post_content);?></p>
                
                </div>
                <?php } else{
					   	echo "Please create a page 'about us' as a sub page of 'home page' ";
					   } 
				$tmp_page_id=get_ID_by_slug('home/our-members');
				if($tmp_page_id!=null){ 
				$tmp_data = get_page( $tmp_page_id ); 
					$tmp_title = $tmp_data->post_title; // Get title
				?>
                <div id="members" class="left">
                <h2 style="background: url(<?php echo get_bloginfo('template_url'); ?>/img/ourMembersSilhouette.png) no-repeat; padding-left: 35px; padding-bottom: 16px; padding-top: 6px; margin: 5px;"><?php echo $tmp_title; ?> </h2>
                <?php echo apply_filters('the_content', $tmp_data->post_content); ?>
                
                </div>
                <?php
				}else{
					   	echo "Please create a page 'Our Members' as a sub page of 'home page' ";
					   } 
				$tmp_page_id=get_ID_by_slug('home/farmers-markets');
				if($tmp_page_id!=null){ 
				$tmp_data = get_page( $tmp_page_id ); 
					$tmp_title = $tmp_data->post_title; // Get title
					?>
                <div id="farmersMarkets" class="left">
                <h2 style="background: url(<?php echo get_bloginfo('template_url'); ?>/img/farmersMarketSun.png) no-repeat; padding-left: 35px; padding-bottom: 16px; padding-top: 6px; margin: 5px;"><?php echo $tmp_title; ?> </h2>
                <?php echo apply_filters('the_content', $tmp_data->post_content); ?>
                
                </div>
                <?php }else{
					   	echo "Please create a page 'farmer's markets' as a sub page of 'home page' ";
					   }  ?>
                </div>
                <div id="about" class="clear" style="height: 200px; padding: 5px 5px 5px 35px;">
                    <div style="width: 50%; float:left; overflow:hidden;">
                  <?php $about_page_id=get_ID_by_slug('home/about-colusa-county');
				if($about_page_id!=null){ 
				$about_data = get_page( $about_page_id ); 
					$about_title = $about_data->post_title; // Get title
				?> 
                        <h2 class="red"><?php echo $about_title; ?></h2>
                        <?php echo apply_filters('the_content', $about_data->post_content);  ?>
                       <?php }
					   else{
					   	echo "Please create a page 'about colusa county' as a sub page of 'home page' $about_page_id/".get_page_by_path('home/about-colusa-county');
					   } ?>
                    </div>
                    <div class="slider-wrapper theme-default">
                        <div id="slideshow" class="nivoSlider right" style="margin-left: 10px;">
                       <?php
					   $front_events=get_front_events();
					   if(!empty($front_events)){
						foreach($front_events as $i=>$vars){
							echo "<a href='".$vars['link']."'><img border=0 src='".$vars['image']."' title='".$vars['title']."' alt='".$vars['title']."' width='300'  height='130' /></a>";
						}
						}
                        ?> </div>
                       </div>
                    
                   </div>

			<?php
				
					$page_data = get_page( $page_id ); 
					$content = apply_filters('the_content', $page_data->post_content); 
					$title = $page_data->post_title; // Get title
					if (isset($_COOKIE['cf_mobile']) && $_COOKIE['cf_mobile'] == 'true') {
						//echo $content; // Output Content 
					}
				}else{
					echo "Please create a page titled 'Home', and apply the 'home page' template.";
				}
				?>
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_footer(); ?>
