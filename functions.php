<?php
define('THEMENAME',"colusachamber");
define('CALRSS',get_bloginfo('url').'/category/calendar/feed/');
define('CALICAL',get_bloginfo('url')."?ec3_ical");
define("STICKYID","11");
define("CALID","8");
/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */

if ( ! isset( $content_width ) )
	$content_width = 640;

/** Tell WordPress to run custom_theme_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', 'custom_theme_setup' );

if ( ! function_exists( 'custom_theme_setup' ) ):
function custom_theme_setup() {
	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();
	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );
	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );
	// Make theme available for translation
	// Translations can be filed in the /languages/ directory
	load_theme_textdomain( 'custom_theme', TEMPLATEPATH . '/languages' );
	$locale = get_locale();
	$locale_file = TEMPLATEPATH . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'custom_theme' ),
	) );
	
}
endif;


/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 *
 * To override this in a child theme, remove the filter and optionally add
 * your own function tied to the wp_page_menu_args filter hook.
 *
 * @since Twenty Ten 1.0
 */
function custom_theme_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'custom_theme_page_menu_args' );

/**
 * Sets the post excerpt length to 40 characters.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 *
 * @since Twenty Ten 1.0
 * @return int
 */
function custom_theme_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'custom_theme_excerpt_length' );

/**
 * Returns a "Continue Reading" link for excerpts
 *
 * @since Twenty Ten 1.0
 * @return string "Continue Reading" link
 */
function custom_theme_continue_reading_link() {
	return ' <a href="'. get_permalink() . '">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'custom_theme' ) . '</a>';
}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and custom_theme_continue_reading_link().
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 *
 * @since Twenty Ten 1.0
 * @return string An ellipsis
 */
function custom_theme_auto_excerpt_more( $more ) {
	return ' &hellip;' . custom_theme_continue_reading_link();
}
add_filter( 'excerpt_more', 'custom_theme_auto_excerpt_more' );

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 *
 * @since Twenty Ten 1.0
 * @return string Excerpt with a pretty "Continue Reading" link
 */
function custom_theme_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= custom_theme_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'custom_theme_custom_excerpt_more' );

/**
 * Remove inline styles printed when the gallery shortcode is used.
 *
 * Galleries are styled by the theme in Twenty Ten's style.css.
 *
 * @since Twenty Ten 1.0
 * @return string The gallery style filter, with the styles themselves removed.
 */
function custom_theme_remove_gallery_css( $css ) {
	return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
}
add_filter( 'gallery_style', 'custom_theme_remove_gallery_css' );

if ( ! function_exists( 'custom_theme_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own custom_theme_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Twenty Ten 1.0
 */
function custom_theme_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
		<div class="comment-author vcard">
			<?php echo get_avatar( $comment, 40 ); ?>
			<?php printf( __( '%s <span class="says">says:</span>', 'custom_theme' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
		</div><!-- .comment-author .vcard -->
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em><?php _e( 'Your comment is awaiting moderation.', 'custom_theme' ); ?></em>
			<br />
		<?php endif; ?>

		<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
			<?php
				/* translators: 1: date, 2: time */
				printf( __( '%1$s at %2$s', 'custom_theme' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'custom_theme' ), ' ' );
			?>
		</div><!-- .comment-meta .commentmetadata -->

		<div class="comment-body"><?php comment_text(); ?></div>

		<div class="reply">
			<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div><!-- .reply -->
	</div><!-- #comment-##  -->

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'custom_theme' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __('(Edit)', 'custom_theme'), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}
endif;

/**
 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
 *
 * To override custom_theme_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 *
 * @since Twenty Ten 1.0
 * @uses register_sidebar
 */
function custom_theme_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Primary Widget Area', 'custom_theme' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The primary widget area', 'custom_theme' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
	register_sidebar( array(
		'name' => __( 'Secondary Widget Area', 'custom_theme' ),
		'id' => 'secondary-widget-area',
		'description' => __( 'The secondary widget area', 'custom_theme' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 3, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'First Footer Widget Area', 'custom_theme' ),
		'id' => 'first-footer-widget-area',
		'description' => __( 'The first footer widget area', 'custom_theme' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 4, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Second Footer Widget Area', 'custom_theme' ),
		'id' => 'second-footer-widget-area',
		'description' => __( 'The second footer widget area', 'custom_theme' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 5, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Third Footer Widget Area', 'custom_theme' ),
		'id' => 'third-footer-widget-area',
		'description' => __( 'The third footer widget area', 'custom_theme' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
/** Register sidebars by running custom_theme_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'custom_theme_widgets_init' );

/**
 * Removes the default styles that are packaged with the Recent Comments widget.
 *
 * To override this in a child theme, remove the filter and optionally add your own
 * function tied to the widgets_init action hook.
 *
 * @since Twenty Ten 1.0
 */
function custom_theme_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'custom_theme_remove_recent_comments_style' );

if ( ! function_exists( 'custom_theme_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post—date/time and author.
 *
 * @since Twenty Ten 1.0
 */
function custom_theme_posted_on() {
	printf( __( '<span class="%1$s">Posted on</span> %2$s <span class="meta-sep">by</span> %3$s', 'custom_theme' ),
		'meta-prep meta-prep-author',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
			get_permalink(),
			esc_attr( get_the_time() ),
			get_the_date()
		),
		sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
			get_author_posts_url( get_the_author_meta( 'ID' ) ),
			sprintf( esc_attr__( 'View all posts by %s', 'custom_theme' ), get_the_author() ),
			get_the_author()
		)
	);
}
endif;

if ( ! function_exists( 'custom_theme_posted_in' ) ) :
/**
 * Prints HTML with meta information for the current post (category, tags and permalink).
 *
 * @since Twenty Ten 1.0
 */
function custom_theme_posted_in() {
	// Retrieves tag list of current post, separated by commas.
	$tag_list = get_the_tag_list( '', ', ' );
	if ( $tag_list ) {
		$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'custom_theme' );
	} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
		$posted_in = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'custom_theme' );
	} else {
		$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'custom_theme' );
	}
	// Prints the string, replacing the placeholders.
	printf(
		$posted_in,
		get_the_category_list( ', ' ),
		$tag_list,
		get_permalink(),
		the_title_attribute( 'echo=0' )
	);
}
endif;

function getImages($dir,$imagetypes = array("image/jpeg", "image/gif", "image/png"))
  {
    // array to hold return value
    $retval = array();

    // add trailing slash if missing
    if(substr($dir, -1) != "/") $dir .= "/";

    // full server path to directory
    $fulldir = TEMPLATEPATH . "$dir";
	//echo $fulldir;

    $d = @dir($fulldir) or die("->getImages: Failed opening directory $dir ($fulldir) for reading");
    while(false !== ($entry = $d->read())) {
      // skip hidden files
      if($entry[0] == ".") continue;
      // check for image files
      if(@in_array(mime_content_type("$fulldir$entry"), $imagetypes)) {
        $retval[] = array(
         "file" => "/$dir$entry",
         "size" => getimagesize("$fulldir$entry")
        );
      }
    }
    $d->close();

    return $retval;
  }
  
function load_js_css() {
 
    // only use this method is we're not in wp-admin
    if (!is_admin()) {
 
        // deregister the original version of jQuery
        wp_deregister_script('jquery');
 
        // discover the correct protocol to use
        $protocol='http:';
        if($_SERVER['HTTPS']=='on') {
            $protocol='https:';
        }
 
        // register the Google CDN version
        wp_register_script('jquery', $protocol.'//ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js', false, '1.5.2');
 
        // add it back into the queue
        wp_enqueue_script('jquery');
		
		//if(is_home() || is_front_page()){  //home
		 add_action('wp_print_styles', 'add_my_stylesheet');
		 wp_register_script( 'fadeslideshow', get_bloginfo('template_directory').'/js/fadeslideshow.js');
			wp_enqueue_script( 'fadeslideshow', array('jQuery'));
		 wp_register_script( 'jquery.scroll', get_bloginfo('template_directory').'/js/jquery.scroll.js');
			wp_enqueue_script( 'jquery.scroll', array('jQuery'));
		wp_register_script( 'nivo.slider', get_bloginfo('template_directory').'/js/jquery.nivo.slider.pack.js');
			wp_enqueue_script( 'nivo.slider', array('jQuery'));
		//}
 
    }
	
 
}
add_action('template_redirect', 'load_js_css');

function add_my_stylesheet() {
	//main & reset are already imported through theme css
	$styles=array("nivo-slider"=>"home","scrollbar"=>"home","calendar"=>"calendar");
	$tUrl=get_bloginfo('template_url');
	$tDir=get_bloginfo('template_directory');
	
	foreach($styles as $name=>$page){
		//echo "$name=>$page ishome:'".is_home()."', is_front_page:'".is_front_page()."' page:'".$page."'<br>";
		//if(((is_home() || is_front_page()) && $page=='home') OR ($page!='home') ){
			$myStyleUrl = $tUrl.'/css/'.$name.'.css';
			$myStyleFile = $tDir.'/css/'.$name.'.css';
			//if ( file_exists($myStyleFile) ) {
				wp_register_style(THEMENAME.'_'.$name, $myStyleUrl);
				wp_enqueue_style( THEMENAME.'_'.$name);
			//}
		//}
	}
}

if(!function_exists ( 'get_ID_by_slug')){
	function get_ID_by_slug($page_slug) {
		$page = get_page_by_path($page_slug);
		if ($page) {
			return $page->ID;
		} else {
			return null;
		}
	}
}
 
 /* calendar ajax*/
 
function implement_ajax() {
	if(isset($_POST['calendar_items'])){
			$eventStr="";
			$events = $wpdb->get_results("SELECT `sched_id`,`post_id`,`start` FROM `wp_ec3_schedule` join `wp_posts` on `wp_ec3_schedule`.`post_id`=`wp_posts`.`ID` WHERE `wp_posts`.`post_status`='publish' && `wp_posts`.`post_type`='post'");
			foreach ($events as $event) {
				//"EventID": 1, "Date": "new Date(2010, 11, 1)", "Title": "10:00 pm - EventTitle1", "URL": "#", "Description": "This is a sample event description", "CssClass": "Birthday" },
				$post= get_post($event->post_id);
				$start=explode(" ",$event->start);
				$startDate=$start[0];
				$eventStr.= "\n" .'{ "EventID": '.$event->sched_id.', "Date": "new Date(2010, 11, 1)", "Title": "'.substr(str_replace(",", "",$post->post_title),0,22).'...", "URL": "'.get_bloginfo(url).'/'.$post->post_name.'", "Description": "'.htmlspecialchars(str_replace(",", "",str_replace(chr(13), " ",str_replace(chr(10), " ",nl2br($post->post_content))))).'", "CssClass": "default" },';
				}
			echo substr($eventStr,0,-1)."\n";
			die();
	} // end if
}
add_action('wp_ajax_my_special_ajax_call', 'implement_ajax');
add_action('wp_ajax_nopriv_my_special_ajax_call', 'implement_ajax');

function syndication(){?>
<div style="text-align:center; width:10%;float:right;">
<ul id="calendarNav" style="list-style:none; text-align:center; margin-left:auto; margin-right:auto;">
<li><a href="<?php echo CALRSS; ?>"><img src="<?php  echo get_bloginfo('template_directory'); ?>/img/cal/rss_orange.png" alt="Rss Calendar Feed" width="16" height="16" border="0" /></a></li>
<li><a href="<?php echo CALICAL; ?>"><img src="<?php  echo get_bloginfo('template_directory'); ?>/img/cal/iCal.png" alt="iCal" width="16" height="16" border="0" /></a></li>
</ul></div>
<?php }

function get_category_id($cat_name){
	$term = get_term_by('name', $cat_name, 'category');
	return $term->term_id;
}

function get_front_events(){
global $wpdb;
$wpdb->show_errors();
$itemcount=10;
$querystr="SELECT distinct wposts.ID, wposts.post_content, wposts.post_title, wposts.post_excerpt,cal.end,cal.start
FROM wp_posts wposts
Left JOIN wp_ec3_schedule cal on cal.post_id= wposts.ID
WHERE  
wposts.post_status='publish'
&& (
(wposts.ID in (SELECT object_id FROM `wp_term_relationships` where `wp_term_relationships`.`term_taxonomy_id` in (".CALID.") && cal.end > CURDATE())) 
OR 
(wposts.ID in (SELECT object_id FROM `wp_term_relationships` where `wp_term_relationships`.`term_taxonomy_id` in (".STICKYID.")))
)
ORDER BY  `cal`.`start` ASC 
LIMIT 0 , $itemcount";
$posts = $wpdb->get_results($querystr);
 
$width = "560"; $height = "290";
if (!empty($posts)): 
	global $post; 
	$ret_arr=array();
		foreach ($posts as $post): 
			setup_postdata($post);
			preg_match ('/(src)=("[^"]*")/i',$post->post_content, $matches);
			$first_image=trim($matches[2],'"');
			$temp=array("link"=>get_permalink(), "title"=>the_title("","",false),"image"=>"$first_image");
			array_push($ret_arr,$temp);
		endforeach; 
endif;

return $ret_arr;		
}

?>