<?php
  /* 

 Template Name: Announcements
 */

  get_header();
?>

		<div id="container">
			<div id="content" role="main">
<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'custom_theme' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php  ucwords(the_title()); ?></a></h2>

<?php
$page_data = get_page($post->ID); 
				$content = apply_filters('the_content', $page_data->post_content); 
				$title = $page_data->post_title; // Get title
				echo $content; // Output Content
?><div style="width:90%;margin-left:auto; margin-right:auto;"><?php
$parent=get_category_id('announcements');
$args=array(
	'parent'=>$parent,
  'orderby' => 'name',
  'order' => 'ASC'
  );
$categories=get_categories($args);
  foreach($categories as $category) { 
    echo '<h3><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </h3> ';
    echo '<blockquote>'. $category->description . '</blockquote>';
	}
?>
</div>
			</div><!-- #content -->
		</div><!-- #container -->

<?php 
get_sidebar();
get_footer(); 

?>