<?php
define("IS_MOBILE",FALSE);
get_header(); ?>

		<div id="container">
			<div id="content" role="main">

			<?php
            if(is_home() || is_front_page()){ 
				$page_id=get_ID_by_slug('home');
				$page_data = get_page( $page_id ); 
				$content = apply_filters('the_content', $page_data->post_content); 
				$title = $page_data->post_title; // Get title
				echo $content; // Output Content
			}else{
			/* Run the loop to output the posts.
			 * If you want to overload this in a child theme then include a file
			 * called loop-index.php and that will be used instead.
			 */
			 get_template_part( 'loop', 'index' );
			}
			?>
			</div><!-- #content -->
		</div><!-- #container -->

<?php if(!is_home() && !is_front_page()){ 
		get_sidebar(); 
	} ?>
<?php get_footer(); ?>
