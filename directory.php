<?php
  /* 
 Template Name: Directory
 */
?>
<?php get_header(); ?>

		<div id="container">
			<div id="homepage" role="main">
            	<ul id="membersList">
				<?php
                /*
                    First we set how we'll want to sort the user list.
                    You could sort them by:
                    ------------------------
                    * ID - User ID number.
                    * user_login - User Login name.
                    * user_nicename - User Nice name ( nice version of login name ).
                    * user_email - User Email Address.
                    * user_url - User Website URL.
                    * user_registered - User Registration date.
                */
                $szSort = "user_nicename";
                /*
                    Now we build the custom query to get the ID of the users.
                */
				$qry=$wpdb->prepare(
                    "SELECT $wpdb->users.ID FROM $wpdb->users ORDER BY %s ASC"
                    , $szSort );
				//echo $qry."<br>";
                $aUsersID = $wpdb->get_col($qry);
                /*
                    Once we have the IDs we loop through them with a Foreach statement.
                */
                foreach ( $aUsersID as $iUserID ) :
                /*
                    We use get_userdata() function with each ID.
                */
                $user = get_userdata( $iUserID );
                /*
                    Here we finally print the details wanted.
                    Check the description of the database tables linked above to see
                    all the fields you can retrieve.
                    To echo a property simply call it with $user->name_of_the_column.
                    In this example I print the first and last name.
                */
				if($user->user_login!='admin' && $user->business_name!=''){
				?>
               <div class="business_listing">
			   <h2><?php echo $user->business_name; ?></h2>
               <blockquote>
                
				<?php if(!empty($user->address)){ ?>
				<?php echo $user->address; ?><br />
                <?php echo $user->city; ?>, <?php echo $user->state; ?> <?php echo $user->zip; ?><br />
                <?php } //end address ?>
                
                 <?php if(!empty($user->phone)){ ?>
                <?php echo $user->phone; ?>   Fax: <?php echo $user->fax; ?><br />
                <?php } ?>
				
                 <?php if(!empty($user->first_name)){ ?>
                Contact: <?php echo ucwords( strtolower( $user->first_name . ' ' . $user->last_name ) ); ?><br />
                <?php } ?>
                
                <?php if(!empty($user->user_url)){ 
						$bus_url=$user->user_url;
						if(strpos($bus_url, 'http://')===false){
							$bus_url='http://'.$bus_url;
						}
				?>
                <a href="<?php echo $bus_url; ?>"><?php echo $bus_url; ?></a><br />
                <?php } ?>
                  
                <?php if(!empty($user->member_since)){ ?>
                   Member Since <?php echo $user->member_since; ?>
                 <?php } ?>
                  </blockquote>
                 </div>
                <?php 
				} //end foreach
                /*
                     The strtolower and ucwords part is to be sure
                     the full names will all be capitalized.
                */
                endforeach; // end the users loop.
                ?>
                </ul>
            </div><!-- #content -->
		</div><!-- #container -->

<?php get_footer(); ?>
