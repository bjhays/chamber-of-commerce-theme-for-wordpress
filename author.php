<?php
/*
Template Name: Author Template
*/

global $wp_query;
$curauth = $wp_query->get_queried_object();

$key="wpum_"."business_name";
$business_name = get_usermeta($curauth->ID, $key);

$key="wpum_"."owner";
$business_owner = get_usermeta($curauth->ID, $key);

get_header(); ?>

<div id="bodycontent" >
	<div class="bodytext" >
		<h2><?php echo $business_name; ?></h2>
		<?php if ($business_owner != null) { echo "<p>The owner of this business is ".$business_owner."</p>"; }; ?>
		<p>Website: <a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a></p>
		<p>About the Business: <?php echo $curauth->description; ?></p>
		</div><!-- end div.bodytext -->
	</div><!-- end div.bodycontent -->
	
<?php get_footer(); ?>