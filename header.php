<?php
/**
 * The Header for our theme.
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'custom_theme' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php 
	//wp_register_script("jquery"); 
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script>
jQuery(document).ready(function($) {			
			//scroll bars
			$('#footer>#colophon>div').scrollbar();
			//header background
			var mygallery=new fadeSlideShow({
				wrapperid: "fadeshow1", //ID of blank DIV on page to house Slideshow
				dimensions: [970, 200], //width/height of gallery in pixels. Should reflect dimensions of largest image
				imagearray: [
				<?php 
					$img_dir="/img/header/";
					$images = getImages($img_dir);
					$i=0; 
					$out="";
					foreach($images as $img) {
						//echo "<img class=\"photo\" src=\"{$img['file']}\" {$img['size'][3]} alt=\"\">\n";
						//echo "bgimages[".$i."]='http://localhost".$img['file']."';"."\n";
						$imgurl=get_bloginfo( 'template_url').$img['file'];
						$link="http://google.com";
						$caption="";
						$out.='["'.$imgurl.'", "'.$link.'", "", "'.$caption.'"],'."\n";
						$i++;
					}
					echo substr($out,0,-2);
					?>
					],
				displaymode: {type:'auto', pause:2500, cycles:0, wraparound:false},
				persist: false, //remember last viewed slide and recall within same session?
				fadeduration: 500, //transition duration (milliseconds)
				descreveal: "ondemand",
				togglerid: ""
			});
			//ads slideshow
			$('#slideshow').nivoSlider();
	});
</script>

</head>

<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
	<div id="header">
		<div id="access" role="navigation">
			  <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
				<div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'custom_theme' ); ?>">
					<?php _e( 'Skip to content', 'custom_theme' ); ?></a>
                </div>
				<?php /* Our navigation menu. Default falls back to wp_page_menu.*/ ?>
				<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
			</div><!-- #access -->
            <div id="WelcomeToColusa">
                <div id="brand"><strong>Welcome to the Colusa County Chamber of Commerce Online</strong><br />
                <img src="<?php bloginfo('template_url') ;?>/img/logo.png" alt="Colusa County Chamber of Commerce" width="400" height="156"/></div>
                <div id="fadeshow1" style="position:absolute; z-index:0;"></div>
         	</div><!-- #WelcomeToColusa -->

	</div><!-- #header -->

	<div id="main">
