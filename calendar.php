<?php
  /* 

 Template Name: Calendar
 */

  wp_enqueue_script('jquery');
  get_header();
?>
<style type="text/css" media="screen">
		#jMonthCalendar .default { background-color: #DDFFFF;}
		#jMonthCalendar .Birthday { background-color: #DD00FF;}
		#jMonthCalendar #Event_3 { background-color:#0000FF; }
			/*override theme settings*/
		#main{
			padding:10px;
			}
	</style>
    <script src="<?php  echo get_bloginfo('template_directory'); ?>/js/jMonthCalendar.js" type="text/javascript"></script>
<?php  syndication(); ?>
<div class="calendar">
<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'custom_theme' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
    <script type="text/javascript">
       // jQuery.noConflict(); // start substituting $ for jQuery
		 jQuery().ready(function() {
			var options = {
				height: 600,
				width: 800,
				navHeight: 25,
				labelHeight: 25,
				onMonthChanging: function(dateIn) {
					return true;
				},
				onEventLinkClick: function(event) { 
					//alert("event link click");
					return true; 
				},
				onEventBlockClick: function(event) { 
					//alert("block clicked");
					return true; 
				},
				onEventBlockOver: function(event) {
					//alert(event.Title + " - " + event.Description);
					return true;
				},
				onEventBlockOut: function(event) {
					return true;
				},
				onDayLinkClick: function(date) { 
					//alert(date.toLocaleDateString());
					return true; 
				},
				onDayCellClick: function(date) { 
					//alert(date.toLocaleDateString());
					return true; 
				}
			};
			
			
			var events = [ 	<?php
			$eventStr="";
			$events = $wpdb->get_results("SELECT `sched_id`,`post_id`,`start`,`post_content` FROM `wp_ec3_schedule` join `wp_posts` on `wp_ec3_schedule`.`post_id`=`wp_posts`.`ID` WHERE `wp_posts`.`post_status`='publish' && `wp_posts`.`post_type`='post' && DATE(`end`)>=DATE_SUB(NOW(), INTERVAL 30 DAY)");
			foreach ($events as $event) {
				$post= get_post($event->post_id);
				$start=explode(" ",$event->start);
				$startDate=explode("-",$start[0]);
				$eventStr.= "\n" .'{ "EventID": '.$event->sched_id.', "Date": "new Date('.$startDate[0].', '.(($startDate[1])-1).', '.$startDate[2].')", "Title": "<div onMouseOver=showTip('.addslashes($event->sched_id).') onMouseOut=showTip('.$event->sched_id.')>'.str_replace("\'", "",str_replace("\"", "",substr(str_replace(",", "",$post->post_title),0,22))).'...<span id=tip_'.$event->sched_id.'>'.htmlspecialchars(str_replace("&nbsp;", " ", str_replace("\r\n", "", strip_tags($event->post_content)))).'</span></div>", "URL": "'.get_bloginfo(url).'/'.$post->post_name.'", "Description": "", "CssClass": "default" },';
			}
			echo substr($eventStr,0,-1)."\n";
				?>];
					
			 jQuery.jMonthCalendar.Initialize(options, events);
			
		       });
			   function showTip(id){
			   		var el = document.getElementById("tip_"+id);
					
					if ( el.style.display == 'block' ) {
						el.style.display = 'none';
					}
					else {
						el.style.display = 'block';
					}
			   }
    </script>

<style type="text/css"> 
.Event span{ display:none; 
			margin-left: -999em;
			position: absolute;
			border-radius: 5px 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; 
			box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1); -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1); -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
			position: absolute; left: 1em; top: 2em; z-index: 99;
			margin-left: 0; width: 250px;
			 padding: 0.8em 1em;
			 background: #FFFFAA; border: 1px solid #FFAD33; 
			 }
			 .Event{position: relative;}
			 #calendarNav li{
				padding: 5px;
				vertical-align:middle;
				font-size:20px;
				display:inline;
			 }
</style>
<br />
<center>
		<div id="jMonthCalendar"></div>
</center>

</div><!--end narrowcolumn-->

<?php get_footer(); 

?>