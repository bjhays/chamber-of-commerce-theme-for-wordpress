<?php
  /*
 Template Name: Contact Us
 */
  get_header();
?>
<style>
#contact_form{
	float:left;
	width:400px;
	height:500px;
	margin-left:10px;
	text-align:left;
	padding:10px;
	-moz-border-radius:8px;
	-webkit-border-radius:8px;
	-opera-border-radius:8px;
	-khtml-border-radius:8px;
	border-radius:8px;
	background: #999; /* for non-css3 browsers */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#cccccc', endColorstr='#ffffff'); /* for IE */
	background: -webkit-gradient(linear, left top, left bottom, from(#ccc), to(#fff)); /* for webkit browsers */
	background: -moz-linear-gradient(top,  #ccc,  #fff); /* for firefox 3.6+ */ 
	}
#contactMe td{
	vertical-align:top;
	text-align:right;
}

#contact_message h3{
	font-variant:small-caps;
	font-weight:bold;
	}
#contact_message{
	float:left;
	width:200px;
	height:400px;
	margin-left:100px;
	
}
.page_content{
	margin-left:30px;
}
.entry-title{
	padding-top:15px;
	height:55px;
	padding-left:70px;
	background: no-repeat left top url("<?php  echo get_bloginfo('template_directory'); ?>/img/email_us64.png");
}
</style>
<?php while ( have_posts() ) : the_post(); ?>
<div class="page_content" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'custom_theme' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>			
<br />
<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'custom_theme' ) ); ?>
<?php endwhile; ?>
<?php get_footer(); 

?>