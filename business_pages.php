<?php
  /* 
 Template Name: Business Page
 */
?>
<?php get_header(); ?>
		<div id="container">
			<div id="homepage" role="main">
            		<?php while ( have_posts() ) : the_post(); ?>
                    <?php the_content(); ?>
                    <?php endwhile; ?>
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_footer(); ?>
