	</div><!-- #main -->

	<div id="footer" role="contentinfo">
		
        <div id="colophon">
			<div id="logo">
            	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('first-footer-widget-area') ) : endif; ?>
            </div>
            <div id="contact">
                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('second-footer-widget-area') ) : endif; ?>
            </div>
            <div id="boardMembers" class="fff plain no-arrows">
            	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('third-footer-widget-area') ) : endif; ?>
            </div>
      <div style="clear:both"></div>
    </div>
		</div><!-- #colophon -->
	</div><!-- #footer -->

</div><!-- #wrapper -->

<div class="sub_footer" style="float:left; margin-leftt:20px;"><?php if (function_exists('cfmobi_mobile_link')) { 
		cfmobi_mobile_link(); 
	} ?></div>
<?php

	
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
<div class="sub_footer"  style="float:right; margin-right:20px;"><a href="http://www.NextStepSolutions.net" target="_blank" alt="website by Next Step Solutions">Next Step Solutions</a></div>
</body>
</html>
